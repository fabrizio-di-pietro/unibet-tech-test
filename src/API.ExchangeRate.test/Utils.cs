﻿using API.ExchangeRate.DataProviders.FixerIO;
using API.ExchangeRate.Models;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;

namespace API.ExchangeRate.test
{
    public static class Utils
    {
        public static FixerIOExchangeRates GetMockExchangeRateFeedSuccess()
        {
            return new FixerIOExchangeRates
            {
                Success = true,
                Timestamp = 1569706746,
                BaseCurrency = "EUR",
                Date = "2019-09-28",
                Rates = new Dictionary<string, decimal>(10)
                {
                    { "CAD", 1.449463m },
                    { "CHF", 1.084109m },
                    { "EUR", 1m },
                    { "GBP", 0.890349m },
                    { "XXX", 0.0m }
                }
            };
        }

        public static FixerIOExchangeRates GetMockExchangeRateFeedError()
        {
            return new FixerIOExchangeRates
            {
                Success = false,
                Error = new FixerIOExchangeRates.FixerIOError()
                {
                    Code = 104,
                    Info = "Your monthly API request volume has been reached. Please upgrade your plan."    
                }
            };
        }

        public static IOptions<AppSettings> GetMockAppSettings()
        {
            return Options.Create(new AppSettings()
            {
                CacheDefaultLocation = "127.0.0.1:6379",
                CacheDefaultInstanceName = "FDP",
                CacheDefaultSuccessTimeout = 3600000,
                CacheDefaultErrorTimeout = 0,
                FixerIOKey = "",
                SupportedCurrencies = new string[] { "AUD", "SEK", "USD", "GBP", "EUR" }
            });
        }

		public static CurrencyExchangeResponse GetMockCurrencyExchangeResponse(string baseCurrency, string targetCurrency, decimal exchangeRate)
		{
			return new CurrencyExchangeResponse()
			{
				BaseCurrency = baseCurrency,
				TargetCurrency = targetCurrency,
				ExchangeRate = Math.Round(exchangeRate, 5),
				Timestamp = (new DateTime(1984, 5, 7)).ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'ffK")
			};
		}

		public static CurrencyExchangeResponse GetMockCalculatedCurrencyExchangeResponse(string baseCurrency, string targetCurrency)
		{
			FixerIOExchangeRates fioe = GetMockExchangeRateFeedSuccess();
			fioe.Rates.TryGetValue(baseCurrency, out decimal baserate);
			fioe.Rates.TryGetValue(targetCurrency, out decimal targetrate);
			return new CurrencyExchangeResponse()
			{
				BaseCurrency = baseCurrency,
				TargetCurrency = targetCurrency,
				ExchangeRate = Math.Round(targetrate/baserate, 5),
				Timestamp = (new DateTime(1984, 5, 7)).ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'ffK")
			};
		}
	}
}
