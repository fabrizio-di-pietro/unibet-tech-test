using API.ExchangeRate.DataProviders;
using API.ExchangeRate.Models;
using API.ExchangeRate.Services;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Moq;
using System;
using System.Threading.Tasks;
using Xunit;

namespace API.ExchangeRate.test.Services
{
    public class CurrencyExchangeServicesTest
    {
        private readonly Mock<ILogger<CurrencyExchangeServices>> _logger;
        private readonly IOptions<AppSettings> _appSettings;
        private readonly Mock<IRedisCacheService> _cache;
        private Moq.Language.Flow.ISetup<IRedisCacheService, Task<CurrencyExchangeResponse>> _cacheSetup;

        private const string baseCurrency = "EUR";
        private const string targetCurrency = "GBP";
        private const decimal exchangeRate = 0.6m;

        public CurrencyExchangeServicesTest()
        {
            _appSettings = Utils.GetMockAppSettings();
            _logger = new Mock<ILogger<CurrencyExchangeServices>>();
            _cache = new Mock<IRedisCacheService>();
            _cacheSetup = _cache.Setup(o => o.GetCacheItemAsync(It.IsAny<string>(), It.IsAny<Func<Task<CurrencyExchangeResponse>>>(), It.IsAny<uint>()));

        }

        [Fact]
        public async Task Test_CorrectConversionRate()
        {
            var currencyExchangeProvider = new Mock<ICurrencyExchangeProviders>();
            currencyExchangeProvider.Setup(o => o.GetExchangeRatesAsync()).Returns(Task.FromResult(Utils.GetMockExchangeRateFeedSuccess()));

            _cacheSetup.Returns(Task.FromResult(Utils.GetMockCurrencyExchangeResponse(baseCurrency, targetCurrency, exchangeRate)));

            var currencyExchangeService = new CurrencyExchangeServices(_appSettings, _logger.Object, _cache.Object, currencyExchangeProvider.Object);

            Assert.Equal(exchangeRate, (await currencyExchangeService.GetConversionRateAsync(baseCurrency, targetCurrency)).ExchangeRate);
        }

		[Fact]
		public async Task Test_NullConversionRate()
		{
			var currencyExchangeProvider = new Mock<ICurrencyExchangeProviders>();
			currencyExchangeProvider.Setup(o => o.GetExchangeRatesAsync()).Returns(Task.FromResult(Utils.GetMockExchangeRateFeedSuccess()));

			_cacheSetup.Returns(Task.FromResult((CurrencyExchangeResponse)null));
			var currencyExchangeService = new CurrencyExchangeServices(_appSettings, _logger.Object, _cache.Object, currencyExchangeProvider.Object);

			await Assert.ThrowsAsync<AggregateException>(async () => await currencyExchangeService.GetConversionRateAsync("TRY", "XXX"));
		}



		/// <summary>
		/// { "CAD", 1.449463m },
		/// { "CHF", 1.084109m },
		/// { "EUR", 1m },
		/// { "GBP", 0.890349m },
		/// </summary>
		/// <returns></returns>

		[Fact]
		public async Task Test_EURGBPConversionRate()
		{
			var currencyExchangeProvider = new Mock<ICurrencyExchangeProviders>();

			currencyExchangeProvider.Setup(o => o.GetExchangeRatesAsync()).Returns(Task.FromResult(Utils.GetMockExchangeRateFeedSuccess()));

			_cacheSetup.Returns(Task.FromResult(Utils.GetMockCurrencyExchangeResponse("EUR", "GBP", 0.89035m)));

			var currencyExchangeService = new CurrencyExchangeServices(_appSettings, _logger.Object, _cache.Object, currencyExchangeProvider.Object);

			var exRate = await currencyExchangeService.GetConversionRateAsync("EUR", "GBP");
			Assert.Equal(0.89035m, exRate.ExchangeRate);
		}

		[Fact]
		public async Task Test_GBPEURConversionRate()
		{
			var currencyExchangeProvider = new Mock<ICurrencyExchangeProviders>();

			currencyExchangeProvider.Setup(o => o.GetExchangeRatesAsync()).Returns(Task.FromResult(Utils.GetMockExchangeRateFeedSuccess()));

			_cacheSetup.Returns(Task.FromResult(Utils.GetMockCurrencyExchangeResponse("GBP", "EUR", 1 / 0.89035m)));

			var currencyExchangeService = new CurrencyExchangeServices(_appSettings, _logger.Object, _cache.Object, currencyExchangeProvider.Object);

			var exRate = await currencyExchangeService.GetConversionRateAsync("GBP", "EUR");
			Assert.Equal(Math.Round(1 / 0.89035m, 5), exRate.ExchangeRate);
		}

		[Fact]
		public async Task Test_EURCADConversionRate()
		{
			var currencyExchangeProvider = new Mock<ICurrencyExchangeProviders>();

			currencyExchangeProvider.Setup(o => o.GetExchangeRatesAsync()).Returns(Task.FromResult(Utils.GetMockExchangeRateFeedSuccess()));

			_cacheSetup.Returns(Task.FromResult(Utils.GetMockCurrencyExchangeResponse("EUR", "CAD", 1.44946m)));

			var currencyExchangeService = new CurrencyExchangeServices(_appSettings, _logger.Object, _cache.Object, currencyExchangeProvider.Object);

			var exRate = await currencyExchangeService.GetConversionRateAsync("EUR", "CAD");
			Assert.Equal(1.44946m, exRate.ExchangeRate);
		}

		[Fact]
		public async Task Test_CADEURConversionRate()
		{
			var currencyExchangeProvider = new Mock<ICurrencyExchangeProviders>();

			currencyExchangeProvider.Setup(o => o.GetExchangeRatesAsync()).Returns(Task.FromResult(Utils.GetMockExchangeRateFeedSuccess()));

			_cacheSetup.Returns(Task.FromResult(Utils.GetMockCurrencyExchangeResponse("CAD", "EUR", 1 / 1.44946m)));

			var currencyExchangeService = new CurrencyExchangeServices(_appSettings, _logger.Object, _cache.Object, currencyExchangeProvider.Object);

			var exRate = await currencyExchangeService.GetConversionRateAsync("CAD", "EUR");
			Assert.Equal(Math.Round(1 / 1.44946m, 5), exRate.ExchangeRate);
		}

		[Fact]
		public async Task Test_GBPCADConversionRate()
		{
			var currencyExchangeProvider = new Mock<ICurrencyExchangeProviders>();

			currencyExchangeProvider.Setup(o => o.GetExchangeRatesAsync()).Returns(Task.FromResult(Utils.GetMockExchangeRateFeedSuccess()));
			_cacheSetup.Returns(Task.FromResult(Utils.GetMockCurrencyExchangeResponse("GBP", "CAD", 1 / 0.61426m)));

			var currencyExchangeService = new CurrencyExchangeServices(_appSettings, _logger.Object, _cache.Object, currencyExchangeProvider.Object);

			var exRate = await currencyExchangeService.GetConversionRateAsync("GBP", "CAD");
			Assert.Equal(Math.Round(1 / 0.61426m, 5), exRate.ExchangeRate);
		}

		[Fact]
		public async Task Test_CADGBPConversionRate()
		{
			var currencyExchangeProvider = new Mock<ICurrencyExchangeProviders>();

			currencyExchangeProvider.Setup(o => o.GetExchangeRatesAsync()).Returns(Task.FromResult(Utils.GetMockExchangeRateFeedSuccess()));
			_cacheSetup.Returns(Task.FromResult(Utils.GetMockCurrencyExchangeResponse("CAD", "GBP", 0.61426m)));

			var currencyExchangeService = new CurrencyExchangeServices(_appSettings, _logger.Object, _cache.Object, currencyExchangeProvider.Object);

			var exRate = await currencyExchangeService.GetConversionRateAsync("CAD", "GBP");
			Assert.Equal(0.61426m, exRate.ExchangeRate);
		}

		[Fact]
		public async Task Test_EURGBPConversionRateCalculated()
		{
			var currencyExchangeProvider = new Mock<ICurrencyExchangeProviders>();

			currencyExchangeProvider.Setup(o => o.GetExchangeRatesAsync()).Returns(Task.FromResult(Utils.GetMockExchangeRateFeedSuccess()));

			_cacheSetup.Returns(Task.FromResult(Utils.GetMockCalculatedCurrencyExchangeResponse("EUR", "GBP")));

			var currencyExchangeService = new CurrencyExchangeServices(_appSettings, _logger.Object, _cache.Object, currencyExchangeProvider.Object);

			var exRate = await currencyExchangeService.GetConversionRateAsync("EUR", "GBP");
			Assert.Equal(0.89035m, exRate.ExchangeRate);
		}

		[Fact]
		public async Task Test_GBPEURConversionRateCalculated()
		{
			var currencyExchangeProvider = new Mock<ICurrencyExchangeProviders>();

			currencyExchangeProvider.Setup(o => o.GetExchangeRatesAsync()).Returns(Task.FromResult(Utils.GetMockExchangeRateFeedSuccess()));

			_cacheSetup.Returns(Task.FromResult(Utils.GetMockCalculatedCurrencyExchangeResponse("GBP", "EUR")));

			var currencyExchangeService = new CurrencyExchangeServices(_appSettings, _logger.Object, _cache.Object, currencyExchangeProvider.Object);

			var exRate = await currencyExchangeService.GetConversionRateAsync("GBP", "EUR");
			Assert.Equal(1.12316m, exRate.ExchangeRate);
		}

		[Fact]
		public async Task Test_EURCADConversionRateCalculated()
		{
			var currencyExchangeProvider = new Mock<ICurrencyExchangeProviders>();

			currencyExchangeProvider.Setup(o => o.GetExchangeRatesAsync()).Returns(Task.FromResult(Utils.GetMockExchangeRateFeedSuccess()));

			_cacheSetup.Returns(Task.FromResult(Utils.GetMockCalculatedCurrencyExchangeResponse("EUR", "CAD")));

			var currencyExchangeService = new CurrencyExchangeServices(_appSettings, _logger.Object, _cache.Object, currencyExchangeProvider.Object);

			var exRate = await currencyExchangeService.GetConversionRateAsync("EUR", "CAD");
			Assert.Equal(1.44946m, exRate.ExchangeRate);
		}

		[Fact]
		public async Task Test_CADEURConversionRateCalculated()
		{
			var currencyExchangeProvider = new Mock<ICurrencyExchangeProviders>();

			currencyExchangeProvider.Setup(o => o.GetExchangeRatesAsync()).Returns(Task.FromResult(Utils.GetMockExchangeRateFeedSuccess()));

			_cacheSetup.Returns(Task.FromResult(Utils.GetMockCalculatedCurrencyExchangeResponse("CAD", "EUR")));

			var currencyExchangeService = new CurrencyExchangeServices(_appSettings, _logger.Object, _cache.Object, currencyExchangeProvider.Object);

			var exRate = await currencyExchangeService.GetConversionRateAsync("CAD", "EUR");
			Assert.Equal(0.68991m, exRate.ExchangeRate);
		}

		[Fact]
		public async Task Test_GBPCADConversionRateCalculated()
		{
			var currencyExchangeProvider = new Mock<ICurrencyExchangeProviders>();

			currencyExchangeProvider.Setup(o => o.GetExchangeRatesAsync()).Returns(Task.FromResult(Utils.GetMockExchangeRateFeedSuccess()));
			_cacheSetup.Returns(Task.FromResult(Utils.GetMockCalculatedCurrencyExchangeResponse("GBP", "CAD")));

			var currencyExchangeService = new CurrencyExchangeServices(_appSettings, _logger.Object, _cache.Object, currencyExchangeProvider.Object);

			var exRate = await currencyExchangeService.GetConversionRateAsync("GBP", "CAD");
			Assert.Equal(1.62797m, exRate.ExchangeRate);
		}

		[Fact]
		public async Task Test_CADGBPConversionRateCalculated()
		{
			var currencyExchangeProvider = new Mock<ICurrencyExchangeProviders>();

			currencyExchangeProvider.Setup(o => o.GetExchangeRatesAsync()).Returns(Task.FromResult(Utils.GetMockExchangeRateFeedSuccess()));
			_cacheSetup.Returns(Task.FromResult(Utils.GetMockCalculatedCurrencyExchangeResponse("CAD", "GBP")));

			var currencyExchangeService = new CurrencyExchangeServices(_appSettings, _logger.Object, _cache.Object, currencyExchangeProvider.Object);

			var exRate = await currencyExchangeService.GetConversionRateAsync("CAD", "GBP");
			Assert.Equal(0.61426m, exRate.ExchangeRate);
		}

	}
}
