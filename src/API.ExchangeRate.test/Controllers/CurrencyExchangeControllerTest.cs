using API.ExchangeRate.Controllers;
using API.ExchangeRate.Models;
using API.ExchangeRate.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Moq;
using System.Threading.Tasks;
using Xunit;

namespace API.ExchangeRate.test.Controllers
{
    public class CurrencyExchangeControllerTest
    {
        private readonly Mock<ILogger<CurrencyExchangeController>> _logger;
        private readonly IOptions<AppSettings> _appSettings;

        private const string baseCurrency = "EUR";
        private const string targetCurrency = "GBP";
        private const decimal exchangeRate = 0.6m;


        public CurrencyExchangeControllerTest()
        {
            _appSettings = Utils.GetMockAppSettings();
            _logger = new Mock<ILogger<CurrencyExchangeController>>();
        }

        [Fact]
        public async Task Test_Correct()
        {

            CurrencyExchangeResponse cer = Utils.GetMockCurrencyExchangeResponse(baseCurrency, targetCurrency, exchangeRate);

            var currencyExchangeService = new Mock<ICurrencyExchangeServices>();
            currencyExchangeService.Setup(o => o.GetConversionRateAsync(baseCurrency, targetCurrency)).Returns(Task.FromResult(cer));

            var currencyExchangeController = new CurrencyExchangeController(_appSettings, _logger.Object, currencyExchangeService.Object);

            var result = (OkObjectResult)await currencyExchangeController.GetExchangeRateAsync(baseCurrency, targetCurrency);

            Assert.Equal(200, result.StatusCode);
            Assert.Equal(cer, result.Value);
        }

        [Fact]
        public async Task Test_NullBaseCurrency()
        {
            string baseCurrency = string.Empty;

            CurrencyExchangeResponse cer = Utils.GetMockCurrencyExchangeResponse(baseCurrency, targetCurrency, exchangeRate);

            var currencyExchangeService = new Mock<ICurrencyExchangeServices>();
            currencyExchangeService.Setup(o => o.GetConversionRateAsync(baseCurrency, targetCurrency)).Returns(Task.FromResult(cer));

            var currencyExchangeController = new CurrencyExchangeController(_appSettings, _logger.Object, currencyExchangeService.Object);

            var result = (BadRequestObjectResult)await currencyExchangeController.GetExchangeRateAsync(baseCurrency, targetCurrency);
            Assert.Equal(400, result.StatusCode);

            Error resultError = (Error)result.Value;
            Assert.Equal("Value cannot be null. (Parameter 'baseCurrency')", resultError.ErrorMessage);
        }

        [Fact]
        public async Task Test_NullTargetCurrency()
        {
            string targetCurrency = string.Empty;

            CurrencyExchangeResponse cer = Utils.GetMockCurrencyExchangeResponse(baseCurrency, targetCurrency, exchangeRate);

            var currencyExchangeService = new Mock<ICurrencyExchangeServices>();
            currencyExchangeService.Setup(o => o.GetConversionRateAsync(baseCurrency, targetCurrency)).Returns(Task.FromResult(cer));

            var currencyExchangeController = new CurrencyExchangeController(_appSettings, _logger.Object, currencyExchangeService.Object);

            var result = (BadRequestObjectResult)await currencyExchangeController.GetExchangeRateAsync(baseCurrency, targetCurrency);

            Assert.Equal(400, result.StatusCode);

            Error resultError = (Error)result.Value;
            Assert.Equal("Value cannot be null. (Parameter 'targetCurrency')", resultError.ErrorMessage);
        }

        [Fact]
        public async Task Test_SameCurrency()
        {
            string baseCurrency = "AUD";
            string targetCurrency = "AUD";

            CurrencyExchangeResponse cer = Utils.GetMockCurrencyExchangeResponse(baseCurrency, targetCurrency, exchangeRate);

            var currencyExchangeService = new Mock<ICurrencyExchangeServices>();
            currencyExchangeService.Setup(o => o.GetConversionRateAsync(baseCurrency, targetCurrency)).Returns(Task.FromResult(cer));

            var currencyExchangeController = new CurrencyExchangeController(_appSettings, _logger.Object, currencyExchangeService.Object);

            var result = (BadRequestObjectResult)await currencyExchangeController.GetExchangeRateAsync(baseCurrency, targetCurrency);

            Assert.Equal(400, result.StatusCode);

            Error resultError = (Error)result.Value;
            Assert.Equal("Values of 'baseCurrency' and 'targetCurrency' must differ. Reference value is 'AUD'.", resultError.ErrorMessage);
        }

        [Fact]
        public async Task Test_SameCurrencyDifferentCase()
        {
            string baseCurrency = "AUD";
            string targetCurrency = "aud";

            CurrencyExchangeResponse cer = Utils.GetMockCurrencyExchangeResponse(baseCurrency, targetCurrency, exchangeRate);

            var currencyExchangeService = new Mock<ICurrencyExchangeServices>();
            currencyExchangeService.Setup(o => o.GetConversionRateAsync(baseCurrency, targetCurrency)).Returns(Task.FromResult(cer));

            var currencyExchangeController = new CurrencyExchangeController(_appSettings, _logger.Object, currencyExchangeService.Object);

            var result = (BadRequestObjectResult)await currencyExchangeController.GetExchangeRateAsync(baseCurrency, targetCurrency);

            Assert.Equal(400, result.StatusCode);

            Error resultError = (Error)result.Value;
            Assert.Equal("Values of 'baseCurrency' and 'targetCurrency' must differ. Reference value is 'AUD'.", resultError.ErrorMessage);
        }


        [Fact]
        public async Task Test_NotSupportedCurrency()
        {
            string baseCurrency = "TRY";
            string targetCurrency = "AUD";

            CurrencyExchangeResponse cer = Utils.GetMockCurrencyExchangeResponse(baseCurrency, targetCurrency, exchangeRate);

            var currencyExchangeService = new Mock<ICurrencyExchangeServices>();
            currencyExchangeService.Setup(o => o.GetConversionRateAsync(baseCurrency, targetCurrency)).Returns(Task.FromResult(cer));

            var currencyExchangeController = new CurrencyExchangeController(_appSettings, _logger.Object, currencyExchangeService.Object);

            var result = (BadRequestObjectResult)await currencyExchangeController.GetExchangeRateAsync(baseCurrency, targetCurrency);

            Assert.Equal(400, result.StatusCode);

            Error resultError = (Error)result.Value;
            Assert.Equal("Currency not supported. (Parameter 'baseCurrency')\r\nActual value was TRY.", resultError.ErrorMessage);
        }

        [Fact]
        public async Task Test_InvalidLengthCurrency()
        {
            string baseCurrency = "Australian Dollar";
            string targetCurrency = "AUD";

            CurrencyExchangeResponse cer = Utils.GetMockCurrencyExchangeResponse(baseCurrency, targetCurrency, exchangeRate);

            var currencyExchangeService = new Mock<ICurrencyExchangeServices>();
            currencyExchangeService.Setup(o => o.GetConversionRateAsync(baseCurrency, targetCurrency)).Returns(Task.FromResult(cer));

            var currencyExchangeController = new CurrencyExchangeController(_appSettings, _logger.Object, currencyExchangeService.Object);

            var result = (BadRequestObjectResult)await currencyExchangeController.GetExchangeRateAsync(baseCurrency, targetCurrency);

            Assert.Equal(400, result.StatusCode);

            Error resultError = (Error)result.Value;
            Assert.Equal("Format of 'baseCurrency' not valid. It must be 3 letters. Reference value is 'Australian Dollar'.", resultError.ErrorMessage);
        }
    }
}
