﻿using API.ExchangeRate.DataProviders;
using API.ExchangeRate.DataProviders.FixerIO;
using API.ExchangeRate.Models;
using API.ExchangeRate.Services;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace API.ExchangeRate.test.DataProviders
{
    public class FixerIOCurrencyExchangeProviderTest
    {
        private readonly Mock<ILogger<FixerIOCurrencyExchangeProvider>> _logger;
        private readonly IOptions<AppSettings> _appSettings;

        public FixerIOCurrencyExchangeProviderTest()
        {
            _appSettings = Utils.GetMockAppSettings();
            _logger = new Mock<ILogger<FixerIOCurrencyExchangeProvider>>();
        }

        /// <summary>
        /// Comment/uncomment if you want to test real data provider is actually working and returning a success
        /// </summary>
        /// <returns></returns>
        /// <remarks>remember data provider limitations</remarks>
/*
        [Fact]
        public async Task Test_RealCorrectRates()
        {
            IOptions<AppSettings> appSettings = GetRealAppSettings();
            var currencyExchangeProvider = new FixerIOCurrencyExchangeProvider(appSettings, _logger.Object);
            var result = await currencyExchangeProvider.GetExchangeRatesAsync();

            Assert.True(result.Success);
        }
*/
        /// <summary>
        /// Comment/uncomment if you want to test real data provider is actually working and returning a success
        /// </summary>
        /// <returns></returns>
        /// <remarks>remember data provider limitations</remarks>
/*
        [Fact]
        public async Task Test_RealErrorResponse()
        {

            var currencyExchangeProvider = new FixerIOCurrencyExchangeProvider(_appSettings, _logger.Object);
            var result = await currencyExchangeProvider.GetExchangeRatesAsync();

            Assert.True(!result.Success);
        }
*/

        [Fact]
        public async Task Test_CorrectResponse()
        {
            var currencyExchangeProvider = new Mock<ICurrencyExchangeProviders>();
            currencyExchangeProvider.Setup(o => o.GetExchangeRatesAsync()).Returns(Task.FromResult(Utils.GetMockExchangeRateFeedSuccess()));
            var result = await currencyExchangeProvider.Object.GetExchangeRatesAsync();

            Assert.True(result.Success);
        }


        [Fact]
        public async Task Test_ErrorResponse()
        {
            var currencyExchangeProvider = new Mock<ICurrencyExchangeProviders>();
            currencyExchangeProvider.Setup(o => o.GetExchangeRatesAsync()).Returns(Task.FromResult(Utils.GetMockExchangeRateFeedError()));
            var result = await currencyExchangeProvider.Object.GetExchangeRatesAsync();

            Assert.False(result.Success);
        }

        [Fact]
        public async Task Test_ErrorResponseCode()
        {
            int errorCode = 104;
            var currencyExchangeProvider = new Mock<ICurrencyExchangeProviders>();
            currencyExchangeProvider.Setup(o => o.GetExchangeRatesAsync()).Returns(Task.FromResult(Utils.GetMockExchangeRateFeedError()));
            var result = await currencyExchangeProvider.Object.GetExchangeRatesAsync();

            Assert.Equal(errorCode, result.Error.Code);
        }


        private static IOptions<AppSettings> GetRealAppSettings()
        {
            return Options.Create(new AppSettings()
            {
                CacheDefaultLocation = "127.0.0.1:6379",
                CacheDefaultInstanceName = "fab-",
                CacheDefaultSuccessTimeout = 3600000,
                CacheDefaultErrorTimeout = 0,
                FixerIOKey = "377f6136b7cedae4243dcd7410d9c74f",
                SupportedCurrencies = new string[] { "AUD", "SEK", "USD", "GBP", "EUR" }
            });
        }
    }
}
