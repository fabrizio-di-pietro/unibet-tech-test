﻿using System;

namespace API.ExchangeRate.Models
{
    public class Error
    {
        public string ErrorMessage { get; set; }
        public Exception? FullException { get; set; }

        public Error()
        { }

        public Error(string errorMessage, Exception e) : this(errorMessage)
        {
#if DEBUG
            // uncomment to run debug
            // keep commented if running tests
            //FullException = e;
#endif
        }

        public Error(Exception e) : this(e.Message)
        {
#if DEBUG
            // uncomment to run debug
            // keep commented if running tests
            //FullException = e;
#endif
        }

        public Error(string errorMessage)
        {
            ErrorMessage = errorMessage;
            FullException = null;
        }
    }
}
