﻿namespace API.ExchangeRate.Models
{
    public interface ICacheItem<out T>
    {
        T GetData();
        bool HasValue { get; }
    }

    public class CacheItem<T> : ICacheItem<T>
    {
        public bool HasValue { get; set; }
        public T Data { get; set; }

        public T GetData()
        {
            return Data;
        }
    }
}