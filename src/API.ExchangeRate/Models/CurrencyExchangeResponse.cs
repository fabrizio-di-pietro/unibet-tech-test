﻿using System.Runtime.Serialization;

namespace API.ExchangeRate.Models
{
    [DataContract]
    public class CurrencyExchangeResponse
    {
        [DataMember(Name = "baseCurrency", IsRequired = true, EmitDefaultValue = false)]
        public string BaseCurrency { get; set; }
        [DataMember(Name = "targetCurrency", IsRequired = true, EmitDefaultValue = false)]
        public string TargetCurrency { get; set; }
        [DataMember(Name = "exchangeRate", IsRequired = true, EmitDefaultValue = false)]
        public decimal ExchangeRate { get; set; }
        [DataMember(Name = "timestamp", IsRequired = true, EmitDefaultValue = false)]
        public string Timestamp { get; set; }
    }
}