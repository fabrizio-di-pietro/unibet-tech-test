﻿using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace API.ExchangeRate.DataProviders.FixerIO
{
    public class FixerIOCurrencyExchangeProvider : ICurrencyExchangeProviders
    {
        private readonly ILogger<FixerIOCurrencyExchangeProvider> _logger;
        private readonly AppSettings _appSettings;

        public FixerIOCurrencyExchangeProvider(IOptions<AppSettings> appSettings, ILogger<FixerIOCurrencyExchangeProvider> logger)
        {
            _appSettings = appSettings.Value;
            _logger = logger;
        }

        /// <summary>
        /// Get rates from fixer.io and converts them to <see cref="FixerIOExchangeRates"/> object
        /// </summary>
        /// <returns></returns>
        /// <remarks>TODO: decouple. Improve by returning and interface and building a converter to custom object.</remarks>
        public async Task<FixerIOExchangeRates> GetExchangeRatesAsync()
        {
            using (var httpClient = new HttpClient())
            {
                try
                {
                    // shape of http://data.fixer.io/api/latest?access_key=YOUR_ACCESS_KEY
                    string refUrl = string.Concat("http://data.fixer.io/api/latest?access_key=", _appSettings.FixerIOKey);

                    HttpResponseMessage response = await httpClient.GetAsync(refUrl);
                    string result = await response.Content.ReadAsStringAsync();

                    return JsonConvert.DeserializeObject<FixerIOExchangeRates>(result);
                }
                catch (Exception e)
                {
                    _logger.LogError(e, "FixerIOCurrencyExchangeProvider.GetExchangeRatesAsync() error");
                    throw e;
                }
            }
        }
    }
}
