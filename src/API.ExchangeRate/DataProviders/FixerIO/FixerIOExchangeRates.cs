﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace API.ExchangeRate.DataProviders.FixerIO
{
/* json sample reference
    {
      "success":true,
      "timestamp":1569706746,
      "base":"EUR",
      "date":"2019-09-28",
      "rates":{
        "AED":4.018865,
        "AFN":85.669434,
        "ALL":121.855809,
        "AMD":520.432593,
        "ZAR":16.582563,
        "ZMK":9848.247216,
        "ZMW":14.412081,
        "ZWL":352.301287
      },
      "error": {
        "code": 104,
        "info": "Your monthly API request volume has been reached. Please upgrade your plan."    
      }
    }
*/
    public class FixerIOExchangeRates
    {
        [JsonProperty("success")]
        public bool Success { get; set; }

        [JsonProperty("timestamp")]
        public int Timestamp { get; set; }

        [JsonProperty("base")]
        public string BaseCurrency { get; set; }

        [JsonProperty("date")]
        public string Date { get; set; }

        [JsonProperty("rates")]
        public Dictionary<string, decimal> Rates { get; set; }

        [JsonProperty("error")]
        public FixerIOError? Error { get; set; }

        public class FixerIOError
        {
            [JsonProperty("code")]
            public int Code { get; set; }

            [JsonProperty("info")]
            public string Info { get; set; }
        }

        public FixerIOExchangeRates() { }
    }
}