﻿using API.ExchangeRate.DataProviders.FixerIO;
using System.Threading.Tasks;

namespace API.ExchangeRate.DataProviders
{
    public interface ICurrencyExchangeProviders
    {
        /*
         * IMPROVEMENT: coupled only to its data provider.
         * Create abstraction layer so that rates are DataProvider independent and data provider can be decaoupled
         * */
        Task<FixerIOExchangeRates> GetExchangeRatesAsync();
    }
}
