﻿namespace API.ExchangeRate
{
    /// <summary>
    /// Class paired to appsettings.json
    /// </summary>
    public class AppSettings
    {
        public string CacheDefaultLocation { get; set; }
        public string CacheDefaultInstanceName { get; set; }
        public uint CacheDefaultSuccessTimeout { get; set; }
        public uint CacheDefaultErrorTimeout { get; set; }
        public string FixerIOKey { get; set; }
        public string[] SupportedCurrencies { get; set; }
    }
}
