﻿using API.ExchangeRate.DataProviders;
using API.ExchangeRate.DataProviders.FixerIO;
using API.ExchangeRate.Models;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace API.ExchangeRate.Services
{
    public interface ICurrencyExchangeServices
    {
        Task<CurrencyExchangeResponse> GetConversionRateAsync(string baseCurrency, string targetCurrency);
    }

    public class CurrencyExchangeServices : ICurrencyExchangeServices
    {
        private readonly ILogger<CurrencyExchangeServices> _logger;
        private readonly IRedisCacheService _cache;
        private readonly AppSettings _appSettings;
        private readonly ICurrencyExchangeProviders _currencyExchangeProvider;

        public CurrencyExchangeServices(IOptions<AppSettings> appSettings, ILogger<CurrencyExchangeServices> logger, IRedisCacheService cache, ICurrencyExchangeProviders currencyExchangeProvider)
        {
            _appSettings = appSettings.Value;
            _logger = logger;
            _cache = cache;
            _currencyExchangeProvider = currencyExchangeProvider;
        }

        /// <summary>
        /// Returns a <see cref="CurrencyExchangeResponse"/> given <paramref name="baseCurrency"/> and <paramref name="targetCurrency"/>
        /// </summary>
        /// <param name="baseCurrency"></param>
        /// <param name="targetCurrency"></param>
        /// <returns></returns>
        public async Task<CurrencyExchangeResponse> GetConversionRateAsync(string baseCurrency, string targetCurrency)
        {
            baseCurrency = baseCurrency.ToUpper();
            targetCurrency = targetCurrency.ToUpper();

            string key = ExchangeKey(baseCurrency, targetCurrency);

            try
            {
                ConcurrentBag<Exception> asyncExceptions = new ConcurrentBag<Exception>();
                var conversionRate = await _cache.GetCacheItemAsync(
                    string.Concat("conversionRate-", key),
                    async () =>
                    {
                        // gets from cache. Can be improved by sharing the property across the object, but concurrency shuold be handled
                        try
                        {
                            FixerIOExchangeRates rates = await GetAvailableRatesAsync();
                            if (rates != null && rates.Success)
                            {
                                var exchangeRates = await GetExchangeRatesAsync(rates);

                                if (exchangeRates.TryGetValue(key, out decimal rate))
                                {
                                    if (rate > 0)
                                    {
                                        var res = new CurrencyExchangeResponse
                                        {
                                            BaseCurrency = baseCurrency,
                                            TargetCurrency = targetCurrency,
                                            ExchangeRate = Math.Round(rate, 5),
                                            Timestamp = DateTime.UtcNow.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'ffK") // or using date from api/rates object? not clear by requirements
                                        };
                                        return res;
                                    }

                                    string rateMessage = string.Concat("Invalid rate for '", baseCurrency, "-", targetCurrency, "'");
                                    _logger.LogError(rateMessage);
                                    ArgumentOutOfRangeException outOfRangeException = new ArgumentOutOfRangeException(rateMessage);
                                    asyncExceptions.Add(outOfRangeException);
                                    throw outOfRangeException;
                                }

                                string message = string.Concat("Missing key '", baseCurrency, "-", targetCurrency, "'");
                                _logger.LogError(message);
                                KeyNotFoundException keyNotFoundException = new KeyNotFoundException(message);
                                asyncExceptions.Add(keyNotFoundException);
                                throw keyNotFoundException;
                            }
                            else
                            {
                                if (!rates.Success)
                                {
                                    string message = (rates.Error != null) ? string.Concat("DataProvider error code ", rates.Error.Code, ". ", rates.Error.Info) : "Unknown error";

                                    _logger.LogError(message);
                                    Exception fixerIoError = new Exception(message);
                                    asyncExceptions.Add(fixerIoError);
                                    throw fixerIoError;
                                }
                                else
                                {
                                    string message = "DataProvider Unknown error";

                                    _logger.LogError(message);
                                    Exception genericError = new Exception(message);
                                    asyncExceptions.Add(genericError);
                                    throw genericError;
                                }
                            }
                        }
                        catch (Exception e)
                        {
                            asyncExceptions.Add(e);
                            throw e;
                        }
                    },
                    _appSettings.CacheDefaultSuccessTimeout
                );

                if (conversionRate == null)
                {
                    asyncExceptions.Add(new Exception("Conversion rate is null."));
                }

                if (asyncExceptions.Count > 0)
                {
                    throw new AggregateException(asyncExceptions);
                }

                return conversionRate;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Get available rates from data provider/data source and caches result
        /// </summary>
        /// <returns></returns>
        private async Task<FixerIOExchangeRates> GetAvailableRatesAsync()
        {
            // retrieved data is stored in cache for future purposes and/or reuse due to limited requests/month
            var er = await _cache.GetCacheItemAsync(
                "availableExchangeRates",
                async () => await _currencyExchangeProvider.GetExchangeRatesAsync(),
                _appSettings.CacheDefaultSuccessTimeout
            );

            if (!er.Success || er.BaseCurrency == null)
            {
                // TODO: improve expiration policy if an error occur. This is more of a workaround for the given architecture
                await _cache.UpdateCacheItemExpirationAsync<FixerIOExchangeRates>("availableExchangeRates", 0); // expire immediately.
            }

            return er;
        }

        /// <summary>
        /// Calls underlying <see cref="GetSupportedValuesAsync"/> and caches result
        /// </summary>
        /// <returns></returns>
        private async Task<IDictionary<string, decimal>> GetExchangeRatesAsync(FixerIOExchangeRates rates)
        {
            return await _cache.GetCacheItemAsync(
                "supportedExchangeRates",
                async () => await GetSupportedValuesAsync(rates),
                _appSettings.CacheDefaultSuccessTimeout
            );
        }

        /// <summary>
        /// Generates and returns supported couples of rates. Slow/complex write (O(n^2)), fast read (O(n)).
        /// </summary>
        /// <returns></returns>
        private async Task<IDictionary<string, decimal>> GetSupportedValuesAsync(FixerIOExchangeRates rates)
        {
            IDictionary<string, decimal> ratesTable = GenerateConversionRatesTable(rates);

            IDictionary<string, decimal> supportedCurrencies = new Dictionary<string, decimal>(10);

            foreach (var aRate in ratesTable)
            {
                // add base rate
                if (aRate.Key.ToUpper() != rates.BaseCurrency.ToUpper())
                {
					supportedCurrencies.TryAdd(ExchangeKey(aRate.Key, rates.BaseCurrency), 1 / aRate.Value);
					supportedCurrencies.TryAdd(ExchangeKey(rates.BaseCurrency, aRate.Key), aRate.Value);
                }
                // add other currencies rate
                foreach (var bRate in ratesTable)
                {
                    if (aRate.Key.ToUpper() != bRate.Key.ToUpper())
                    {
                        supportedCurrencies.TryAdd(ExchangeKey(bRate.Key, aRate.Key), aRate.Value / bRate.Value);
                    }
                }
            }
            return supportedCurrencies;
        }


        /// <summary>
        /// Generates subset of rates accoding to <see cref="_appSettings.SupportedCurrencies" />
        /// </summary>
        /// <returns></returns>
        private IDictionary<string, decimal> GenerateConversionRatesTable(FixerIOExchangeRates rates)
        {
            IDictionary<string, decimal> exchangeRates = new Dictionary<string, decimal>(10);

            if (rates?.Rates != null)
            {
                foreach (string currency in _appSettings.SupportedCurrencies)
                {
                    // still need to ensure that dict key is uppercase while deserializing
                    if (rates.Rates.TryGetValue(currency.ToUpper(), out decimal rate))
                    {
                        exchangeRates.TryAdd(currency.ToUpper(), rate);
                    }
                }
            }
            return exchangeRates;
        }

        /// <summary>
        /// Standard key reference
        /// </summary>
        /// <param name="baseCurrency"></param>
        /// <param name="targetCurrency"></param>
        /// <returns></returns>
        private static string ExchangeKey(string baseCurrency, string targetCurrency)
        {
            return string.Concat(baseCurrency.ToUpper(), "-", targetCurrency.ToUpper());
        }
    }
}
