﻿using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Threading.Tasks;
using API.ExchangeRate.Models;
using System.Collections.Concurrent;

namespace API.ExchangeRate.Services
{
    public interface IRedisCacheService
    {
        Task<T> GetCacheItemAsync<T>(string keyName, Func<Task<T>> action, uint timeout = 60000);
        Task UpdateCacheItemExpirationAsync<T>(string keyName, uint timeout = 10000);
    }

    public class RedisCacheService : IRedisCacheService
    {
        private readonly ILogger<RedisCacheService> _logger;
        private readonly IDistributedCache _cache;
        /// <summary>
        /// Fake multiplexer to handle simultaneous/close identical requests as a protection layer before invoking methods or digging into the cache
        /// </summary>
        /// <remarks>Static so that it is shared across all the requests of the API instance</remarks>
        private static readonly ConcurrentDictionary<string, Task> GlobalRequests = new ConcurrentDictionary<string, Task>();

        public RedisCacheService(ILogger<RedisCacheService> logger, IDistributedCache cache)
        {
            _logger = logger;
            _cache = cache;
        }

        /// <summary>
        /// Function to retrieve item from cache. It relies on underlying <see cref="DoGetCacheItemAsync{T}(string, Func{Task{T}}, uint)"/> and leverages the usage of <see cref="GlobalRequests"/> to handle multiple identical requets limiting the pass-through to 1 request of a type at a time.
        /// </summary>
        /// <typeparam name="T">Data Type return from cache</typeparam>
        /// <param name="key">The cache key reference</param>
        /// <param name="action">Action to invoke if/when key is not present</param>
        /// <param name="timeout">Timeout in milliseconds</param>
        /// <returns></returns>
        public async Task<T> GetCacheItemAsync<T>(string key, Func<Task<T>> action, uint timeout = 0)
        {
            /*
             * Functional note: first request behaves as a straight call to reqested action
             * If an identical further GetCacheItemAsync request comes in before the execution of the first one is completed (and, then, stored in cache),
             * it succesfully ends up in the first condition and awaits for the result to be returned.
             * */

            // combine identical GetCacheItem requests
            if (GlobalRequests.TryGetValue(key, out var request) && request is Task<T>)
            {
                return await (Task<T>)request;
            }

            /*
             * NOTE: fire and forget to allow combining of identical requests on next request if current one is not resolved on time
             * */
            var req = DoGetCacheItemAsync(key, action, timeout);
            request = GlobalRequests.AddOrUpdate(key, req, (k, v) => v);
            if (req == request)
            {
                await req.ContinueWith(t => GlobalRequests.TryRemove(key, out var _)); // resolve result
                return req.Result;
            }
            return await req;
        }

        /// <summary>
        /// Actual function to retrieve item from cache. If item not present, it stores it into the cache using <paramref name="key"/> with <paramref name="timeout"/> value to set the expiration
        /// </summary>
        /// <typeparam name="T">Data Type return from cache</typeparam>
        /// <param name="key">The cache key reference</param>
        /// <param name="action">Action to invoke if/when key is not present</param>
        /// <param name="timeout">Timeout in milliseconds</param>
        /// <returns></returns>
        private async Task<T> DoGetCacheItemAsync<T>(string key, Func<Task<T>> action, uint timeout)
        {
            _logger.LogDebug("Get/set key " + key + " from/to cache");

            // no cache
            if (timeout == 0)
                return await action.Invoke();

            ICacheItem<T> cacheItem;
            try
            {
                cacheItem = await GetCacheAsync<T>(key);
            }
            catch (Exception e)
            {
                // cache system unavailable - log error and pass request through
                _logger.LogWarning(e, string.Concat("Unable to get key ", key, " from cache at ", DateTime.Now, ". Pass through."));
                return await action.Invoke();
            }

            // item not found in cache
            if (cacheItem == null)
            {
                var requestedItem = await action.Invoke();
                try
                {
                    cacheItem = requestedItem == null
                        ? await AddCacheAsync<T>(key, default, timeout, false)
                        : await AddCacheAsync(key, requestedItem, timeout, true);
                }
                catch (Exception e)
                {
                    _logger.LogWarning(e, string.Concat("Unable to set key ", key, " in cache at ", DateTime.Now, ". Pass through."));
                    return requestedItem == null
                        ? default
                        : requestedItem;
                }
            }

            return cacheItem != null && cacheItem.HasValue
                ? cacheItem.GetData()
                : default;
        }

        /// <summary>
        /// Reduce the expiration timeout of <paramref name="key"/> to <paramref name="timeout"/> millisecond from now
        /// </summary>
        /// <typeparam name="T">Caceh item Data Type</typeparam>
        /// <param name="key">The cache key reference</param>
        /// <param name="timeout">Timeout in milliseconds</param>
        /// <returns></returns>
        public async Task UpdateCacheItemExpirationAsync<T>(string key, uint timeout = 10000)
        {
            try
            {
                ICacheItem<T> cacheItem = await GetCacheAsync<T>(key);

                if (cacheItem != null)
                {
                    // item found: leverage AddCacheAsync to overwrite the key with different timeout 
                    await AddCacheAsync(key, cacheItem, timeout, true);
                }
            }
            catch (Exception e)
            {
                // cache system unavailable - log error and continue excution
                _logger.LogError(e, "Cache system unavailable");
            }
        }


        /// <summary>
        /// Function to add item into cache
        /// </summary>
        /// <typeparam name="T">Data Type add to cache</typeparam>
        /// <param name="key">Cache key name</param>
        /// <param name="valueObject">Object that will be serialized and added to cache</param>
        /// <param name="timeout">Timeout for cache</param>
        /// <param name="hasValue">Indicate if there is data</param>
        /// <returns></returns>
        private async Task<CacheItem<T>> AddCacheAsync<T>(string key, T valueObject, uint timeout, bool hasValue = true)
        {
            _logger.LogDebug("Add key " + key + " to cache");
            var cacheItem = new CacheItem<T>
            {
                Data = valueObject,
                HasValue = hasValue
            };

            var value = JsonConvert.SerializeObject(cacheItem);
            await _cache.SetStringAsync(key, value, new DistributedCacheEntryOptions() { AbsoluteExpirationRelativeToNow = TimeSpan.FromMilliseconds(timeout) });

            return cacheItem;
        }

        /// <summary>
        /// Function to retrieve item from cache
        /// </summary>
        /// <typeparam name="T">Data Type return from cache</typeparam>
        /// <param name="key">The cache key reference</param>
        /// <returns></returns>
        private async Task<ICacheItem<T>> GetCacheAsync<T>(string key)
        {
            _logger.LogDebug("Get key " + key + " from cache");
            string result = await _cache.GetStringAsync(key);
            if (result != null)
            {
                return JsonConvert.DeserializeObject<CacheItem<T>>(result);
            }
            return null;
        }
    }
}
