﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using API.ExchangeRate.Models;
using API.ExchangeRate.Services;

namespace API.ExchangeRate.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [Produces("application/json")]
    public class CurrencyExchangeController : BaseController
    {
        private readonly ILogger<CurrencyExchangeController> _logger;
        private readonly ICurrencyExchangeServices _currencyExchangeServices;
        private readonly AppSettings _appSettings;

        public CurrencyExchangeController(IOptions<AppSettings> appSettings, ILogger<CurrencyExchangeController> logger, ICurrencyExchangeServices currencyExchangeServices)
        {
            _appSettings = appSettings.Value;
            _logger = logger;
            _currencyExchangeServices = currencyExchangeServices;
        }

        /// <summary>
        /// Upfront 60s cache result per node. Underlying cache values granted by Redis. Default Redis timout set in appsettings.json
        /// </summary>
        /// <param name="baseCurrency"></param>
        /// <param name="targetCurrency"></param>
        /// <returns></returns>
        [HttpGet, Route("{baseCurrency}/{targetCurrency}")]
        [ResponseCache(Duration = 60)]
        public async Task<IActionResult> GetExchangeRateAsync(string baseCurrency, string targetCurrency)
        {
            IActionResult result;
            try
            {
                ValidateExchangeRateInput(baseCurrency, targetCurrency);

                _logger.LogInformation("Valid request input '{0}' '{1}'.", baseCurrency, targetCurrency);
                CurrencyExchangeResponse conversionRate = await _currencyExchangeServices.GetConversionRateAsync(baseCurrency, targetCurrency);
                if (conversionRate != null)
                {
                    result = Ok(conversionRate);
                }
                else
                {
                    string message = string.Format("No conversion rate has been found for '{0}' '{1}'.", baseCurrency, targetCurrency);
                    _logger.LogError(message);
                    result = NoContent();
                }
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message, e);
                result = BadRequest(new Error(e));
            }

            return result;
        }

        /// <summary>
        /// Validate input parameters
        /// </summary>
        /// <param name="baseCurrency"></param>
        /// <param name="targetCurrency"></param>
        private void ValidateExchangeRateInput(string baseCurrency, string targetCurrency)
        {
            if (string.IsNullOrWhiteSpace(baseCurrency))
            {
                throw new ArgumentNullException(nameof(baseCurrency));
            }

            if (string.IsNullOrWhiteSpace(targetCurrency))
            {
                throw new ArgumentNullException(nameof(targetCurrency));
            }

            baseCurrency = baseCurrency.Trim();
            targetCurrency = targetCurrency.Trim();

            Regex r = new Regex(@"^[a-z]{3}$", RegexOptions.IgnoreCase);
            if (!r.Match(baseCurrency).Success)
            {
                throw new FormatException(string.Concat("Format of '", nameof(baseCurrency), "' not valid. It must be 3 letters. Reference value is '", baseCurrency, "'."));
            }
            if (!r.Match(targetCurrency).Success)
            {
                throw new FormatException(string.Concat("Format of '", nameof(targetCurrency), "' not valid. It must be 3 letters. Reference value is '", targetCurrency, "'."));
            }

            // next 3 solve the requirement: "The cartesian product excluding matching base and target pairs of the following currencies should be supported"
            if (baseCurrency.ToUpper() == targetCurrency.ToUpper())
            {
                throw new ArgumentException(string.Concat("Values of '", nameof(baseCurrency), "' and '", nameof(targetCurrency), "' must differ. Reference value is '", baseCurrency, "'."));
            }

            if (!_appSettings.SupportedCurrencies.Contains(baseCurrency.ToUpper()))
            {
                throw new ArgumentOutOfRangeException(nameof(baseCurrency), baseCurrency, "Currency not supported.");
            }

            if (!_appSettings.SupportedCurrencies.Contains(targetCurrency.ToUpper()))
            {
                throw new ArgumentOutOfRangeException(nameof(targetCurrency), targetCurrency, "Currency not supported.");
            }
        }
    }
}
