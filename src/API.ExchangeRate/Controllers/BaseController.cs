﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace API.ExchangeRate.Controllers
{
    public abstract class BaseController : ControllerBase
    {
        private static readonly JsonSerializerSettings jss = new JsonSerializerSettings()
        {
            ContractResolver = new CamelCasePropertyNamesContractResolver(),
            Formatting = Formatting.None,
            NullValueHandling = NullValueHandling.Ignore
        };

        protected string GetJsonResult<T>(T data)
        {
            return JsonConvert.SerializeObject(data, jss);
        }
    }
}
