using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using API.ExchangeRate.DataProviders;
using API.ExchangeRate.Services;
using API.ExchangeRate.DataProviders.FixerIO;

namespace API.ExchangeRate
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // add app settings service
            var appSettingsConfig = Configuration.GetSection("AppSettings");
            var appSettings = appSettingsConfig.Get<AppSettings>();
            services.Configure<AppSettings>(appSettingsConfig);

            //// add context accessor service
            //// note: required by NLog AspNetLayoutRenderers to access HttpContext
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            // redis distributed cache -- simple version
            services.AddStackExchangeRedisCache(options =>
            {
                options.Configuration = appSettings.CacheDefaultLocation;
                options.InstanceName = appSettings.CacheDefaultInstanceName;
            });

            // enable response caching -- use as top layer caching
            services.AddMemoryCache();
            services.AddResponseCaching();


            // custom  services
            services.AddScoped<IRedisCacheService, RedisCacheService>();
            services.AddScoped<ICurrencyExchangeProviders, FixerIOCurrencyExchangeProvider>();
            services.AddScoped<ICurrencyExchangeServices, CurrencyExchangeServices>();

            services.AddControllers();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            app.UseResponseCaching();

            // enable swagger?
            //app.UseSwaggerUI();
        }
    }
}
